package com.myntra.simple.client.request;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.entries.AbstractEntry;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.simple.client.entry.EnrichProductEntry;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */
@XmlRootElement(name = "enrichmentRequest")
public class EnrichProductRequest extends AbstractEntry {

    @XmlElement(name = "clientId")
    private String clientId;

    @XmlElement(name = "data")
    private String data;

    public EnrichProductRequest(String clientId, String data) {
        this.clientId = clientId;
        this.data = data;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "\"clientId\":\"" + clientId + "\"" +
                ",\"data\":" + data +
                "}";
    }
}
