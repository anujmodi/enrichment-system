package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.entries.AbstractEntry;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.simple.client.entry.EnrichProductEntry;
import com.myntra.simple.client.response.EnricherResponse;

import java.util.List;

/**
 * Created by Aditya Upadhyaya on 02/11/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnrichedProductEntry extends AbstractEntry {

    @JsonProperty("source")
    EnrichProductEntry enrichProductEntry;

    @JsonProperty("enrichers")
    List<EnricherResponse> enricherResponseList;

    public EnrichProductEntry getEnrichProductEntry() {
        return enrichProductEntry;
    }

    public void setEnrichProductEntry(EnrichProductEntry enrichProductEntry) {
        this.enrichProductEntry = enrichProductEntry;
    }

    public List<EnricherResponse> getEnricherResponseList() {
        return enricherResponseList;
    }

    public void setEnricherResponseList(List<EnricherResponse> enricherResponseList) {
        this.enricherResponseList = enricherResponseList;
    }

    @Override
    public String toString() {
        return "{" +
                "\"source\":" + getEnrichProductEntry()  +
                ",\"enrichers\":" + getEnricherResponseList() +
                "}";
    }
}
