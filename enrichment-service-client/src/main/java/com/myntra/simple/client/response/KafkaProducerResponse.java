package com.myntra.simple.client.response;

import com.myntra.commons.response.AbstractResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Aditya Upadhyaya on 27/10/15.
 */
@XmlRootElement(name = "kafkaResponse")
public class KafkaProducerResponse extends AbstractResponse {


   private String reqID;

    public String getReqID() {
        return reqID;
    }

    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    public KafkaProducerResponse(String reqID){
        this.reqID = reqID;
    }

}
