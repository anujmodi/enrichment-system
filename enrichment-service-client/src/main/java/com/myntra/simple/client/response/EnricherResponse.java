package com.myntra.simple.client.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myntra.commons.response.AbstractResponse;
import kafka.utils.Json;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Aditya Upadhyaya on 02/11/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize()
public class EnricherResponse extends AbstractResponse {

    @JsonProperty("version")
    String version;

    @JsonProperty("response")
    JsonNode response;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public JsonNode getResponse() {
        return response;
    }

    public void setResponse(JsonNode response) {
        this.response = response;
    }


    @Override
    public String toString() {
        return "{" +
                "\"status\":" + getStatus()  +
                ",\"version\":" + getVersion() +
                ",\"response\":" + getResponse() +
                "}";
    }
}
