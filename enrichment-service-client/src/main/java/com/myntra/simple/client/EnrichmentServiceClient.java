package com.myntra.simple.client;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.kafka.client.response.KafkaServiceResponse;
import com.myntra.simple.client.request.EnrichProductRequest;
import com.myntra.simple.client.response.EnrichmentJobSubmitResponse;
import org.springframework.stereotype.Component;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */
@Component
public class EnrichmentServiceClient {

    public static final String SERVICE_PREFIX = "/enrich/";

    public KafkaServiceResponse enrichProduct(String serviceUrl, String clientId, String data, ContextInfo info)
            throws ERPServiceException {

        BaseWebClient client = new BaseWebClient(serviceUrl, ServiceURLProperty.MFS_URL, info);
        EnrichProductRequest enrichProductRequest = new EnrichProductRequest(clientId,data);
        client.path(SERVICE_PREFIX + "push/");
        return client.post(enrichProductRequest,KafkaServiceResponse.class);
    }


}
