package com.myntra.simple.client.codes;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * Created by pavankumar.at on 15/07/15.
 */
public class SimpleErrorCodes extends ERPErrorCodes {

    private static final String BUNDLE_NAME = "SimpleErrorCodes";

    public SimpleErrorCodes(int code, String message) {
        setAll(code, message, BUNDLE_NAME);
    }

    public static final StatusCodes JSON_COULD_NOT_BE_PARSED = new SimpleErrorCodes(1008,"JSON_NOT_PARSED");
    public static final StatusCodes IO_ERROR = new SimpleErrorCodes(1009,"IO_ERROR");


}
