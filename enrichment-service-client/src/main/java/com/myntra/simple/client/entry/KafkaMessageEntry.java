package com.myntra.simple.client.entry;

import com.myntra.commons.entries.AbstractEntry;

/**
 * Created by mi0093 on 27/10/15.
 */
public class KafkaMessageEntry extends AbstractEntry {

    private String topic;

    private EnrichProductEntry message;


    public KafkaMessageEntry(String topic, EnrichProductEntry message) {
        this.topic = topic;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public EnrichProductEntry getMessage() {
        return message;
    }

    public void setMessage(EnrichProductEntry message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "KafkaMessageEntry{" +
                "topic='" + topic + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
