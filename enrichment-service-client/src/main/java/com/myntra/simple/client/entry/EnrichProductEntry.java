package com.myntra.simple.client.entry;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.myntra.commons.entries.AbstractEntry;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


public class EnrichProductEntry extends AbstractEntry {

    @JsonProperty("id")
    private String id;

    @JsonProperty("data")
    private String data;


    public EnrichProductEntry(String id, String data) {
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id  +
                ",\"data\":" + data +
                "}";
    }
}