insert into client_workflow values ('fdb001','fashionDB','textEn001',2);
insert into client_workflow values ('fdb001','fashionDB','imgEn001',1);

insert into enrichers values ('textEn001','http://10.148.87.46:7001/AttrTags','rest');
insert into enrichers values ('textEn001','http://10.148.87.46:7002/TaxonomyTransformationv2/mapify/mapper/post','rest');
insert into enrichers values ('imgEn001','http://122.248.19.238:8080/enrich','rest', "NULL", "NULL");

insert into enricher_attr values (1, 'textEn001','text_to_enrich','desc');
insert into enricher_attr values (2, 'imgEn001','colour','color');