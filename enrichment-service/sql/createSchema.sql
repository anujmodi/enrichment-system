CREATE DATABASE IF NOT EXISTS enrichment_system;



USE enrichment_system;



CREATE TABLE IF NOT EXISTS `client_workflow` (
  `id` int auto_increment,
  `client_id` VARCHAR(20) NOT NULL,
  `enricher_id`  VARCHAR(20) NOT NULL ,
  `enricher_name` VARCHAR(20) NOT NULL,
  `flow_order` int(20) NOT NULL,
  primary key(id)
);

CREATE TABLE IF NOT EXISTS `client_configs` (
  `id` int auto_increment,
  `client_id` VARCHAR(20) NOT NULL,
  `client_name` VARCHAR(20) NOT NULL,
  `response_url`  VARCHAR(64) NOT NULL ,
  primary key(id)
);





CREATE TABLE IF NOT EXISTS `enrichers` (
  `enricher_id` VARCHAR(20) NOT NULL,
  `enricher_url` VARCHAR(100) NOT NULL,
  `type` VARCHAR(20) NOT NULL,
   primary key(enricher_id)
);




CREATE TABLE IF NOT EXISTS `enricher_attr` (
 `id` int auto_increment,
  `enricher_id` VARCHAR(20) NOT NULL,
  `key_name` VARCHAR(20) NOT NULL,
  `source_key` VARCHAR(20) NOT NULL,
   primary key(id)
);
