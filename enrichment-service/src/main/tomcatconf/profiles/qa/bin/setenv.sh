export CATALINA_PID="/myntra/pid/simple.pid"
export JRE_HOME="/usr/java/java8"
export JAVA_OPTS="-Xms512m -Xmx1024m -XX:MaxPermSize=256m -Dlog4j.configurationFile=conf/log4j2.xml"
