package com.myntra.simple.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.simple.client.entry.EnrichProductEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class MessageHandler {

    private static final Logger LOGGER = Logger.getLogger(MessageHandler.class);


    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ClientHandler clientHandler;


    public void handleMessage(EnrichProductEntry enrichProductEntry){
        String id = enrichProductEntry.getId();
        String clientId = getClientId(enrichProductEntry.getData());
        String payload = getPayload(enrichProductEntry.getData());
        LOGGER.info("Message Handler got a Message with id : "+id+" and clientId : "+clientId);
        LOGGER.info("Payload is "+payload);
        LOGGER.info("Sending Message to ClientHandler");
        clientHandler.enrich(enrichProductEntry,payload, clientId);
    }



    public String getClientId(String data){
        try{
            JsonNode root = objectMapper.readTree(data);
            String clientId = root.path("clientId").toString();
            LOGGER.info("MessageHandler: Client Id : "+clientId);
            return clientId;
        }catch (Exception e){
            LOGGER.error("MessageHandler : Exception : getClientID(). Message : "+e.getMessage() );
            return null;
        }
    }


    public String getPayload(String data){

        try{
            JsonNode root = objectMapper.readTree(data);
            String payload = root.path("entity").toString();
            return payload;
        }catch (Exception e){
            LOGGER.error("Exception : getPayload(). Message : "+e.getMessage() );
            return null;
        }

    }


}
