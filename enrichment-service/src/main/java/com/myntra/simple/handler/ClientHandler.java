package com.myntra.simple.handler;

import java.util.*;

import com.myntra.simple.client.entry.EnrichProductEntry;
import com.myntra.simple.client.entry.EnrichedProductEntry;
import com.myntra.simple.client.response.EnricherResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myntra.commons.exception.ManagerException;
import com.myntra.simple.entity.ClientConfigEntity;
import com.myntra.simple.entity.ClientWorkflowEntity;
import com.myntra.simple.manager.ClientConfigManager;
import com.myntra.simple.manager.ClientWorkflowManager;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class ClientHandler {

    private static final Logger LOGGER = Logger.getLogger(ClientHandler.class);

    @Autowired
    private ClientWorkflowManager clientWorkflowManager;

    @Autowired
    private ClientConfigManager clientConfigManager;

    @Autowired
    private EnricherHandler enricherHandler;

    Map<String,List<ClientWorkflowEntity>> clientWorkflow=new HashMap<>();
    Map<String,ClientConfigEntity> clientConfig=new HashMap<>();
    


    private List<ClientWorkflowEntity> getWorkflow(String clientId){
        LOGGER.info("Getting Workflow for clientID : "+clientId);
        if(clientWorkflowManager == null){
            LOGGER.error("clientWorkflowManager is null");
        }
        if(clientWorkflow.get(clientId)==null) {
            List<ClientWorkflowEntity> workflow = null;
            try {
                workflow = clientWorkflowManager.getWorkflow(clientId);

            } catch (ManagerException e) {
                e.printStackTrace();
            }
            Collections.sort(workflow);
            clientWorkflow.put(clientId,workflow);
        }
        return clientWorkflow.get(clientId);
    }
    
    private ClientConfigEntity getClientConfig(String clientId){
        LOGGER.info("Getting The Response URL for clientID : "+clientId);
        if(clientConfig.get(clientId)==null) {
            ClientConfigEntity config = null;
            try {
            	config = clientConfigManager.getConfig(clientId);
            } catch (ManagerException e) {
                e.printStackTrace();
            }
            clientConfig.put(clientId,config);
        }
        return clientConfig.get(clientId);
    }


    public void enrich(EnrichProductEntry enrichProductEntry,String data, String clientId){
        //EnrichedProductResponse enrichedProductResponse = new EnrichedProductResponse();
        //enrichedProductResponse.setEnrichProductEntry(enrichProductEntry);
        EnrichedProductEntry enrichedResponse = new EnrichedProductEntry();
        List<EnricherResponse> enricherResponseList = new ArrayList<>();


//        StringBuffer enrichedBuffer = new StringBuffer();
//        enrichedBuffer.append("{");
//        enrichedBuffer.append("\"id\":"+"\""+enrichProductEntry.getId()+"\""+",");
//        enrichedBuffer.append("\"entity\":"+enrichProductEntry.getData());
        for(ClientWorkflowEntity clientWorkflowEntity : getWorkflow(clientId)){
            LOGGER.info("Picking one Entity from Workflow and calling EnrichHandler.enrich() from ClientHandler");
            EnricherResponse enricherResponse = enricherHandler.enrich(enrichProductEntry.getId(),data, clientWorkflowEntity.getEnricherID());
            enricherResponseList.add(enricherResponse);
//            enrichedBuffer.append(",");
//            enrichedBuffer.append("\""+clientWorkflowEntity.getEnricher_name()+"\":");
//            enrichedBuffer.append(enricherResponse);


        }
        enrichedResponse.setEnricherResponseList(enricherResponseList);
        enrichedResponse.setEnrichProductEntry(enrichProductEntry);
        LOGGER.info("Enriched Response from all enrichers is " + enrichedResponse.toString());
      //  enrichedBuffer.append("}");
       // LOGGER.info("Enriched Data is :" +enrichedBuffer.toString());
        handleResponse(enrichedResponse, clientId);
    }
    
    private void handleResponse(EnrichedProductEntry enrichedResponse, String clientId){
        LOGGER.info("Handling Enrichment Response for clientId :" + clientId);
    	Map<String,String> headers=new HashMap<>();
        headers.put("Content-Type","application/json");
        headers.put("Accept","application/json");
        HttpHandler httpHandler=new HttpHandler();
    	httpHandler.post(enrichedResponse, headers, getClientConfig(clientId).getResponseUrl());
    }

}
