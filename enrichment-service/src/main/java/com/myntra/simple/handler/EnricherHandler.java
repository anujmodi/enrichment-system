package com.myntra.simple.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.simple.client.entry.EnricherEntry;
import com.myntra.simple.client.response.EnricherResponse;
import com.myntra.simple.entity.EnricherAttrEntity;
import com.myntra.simple.entity.EnricherEntity;
import com.myntra.simple.manager.EnricherAttrManager;
import com.myntra.simple.manager.EnricherManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class EnricherHandler {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private EnricherManager enricherManager;

    @Autowired
    private EnricherAttrManager enricherAttrManager;

    private Map<String,Map<EnricherEntity,List<EnricherAttrEntity>>> enrichers=new HashMap<>();

    private static final Logger LOGGER = Logger.getLogger(EnricherHandler.class);

    public Map<EnricherEntity,List<EnricherAttrEntity>> getEnricher(String enricherId){
        LOGGER.info("EnricherHandler.getEnricher called with enricherId : "+enricherId);
        if(enrichers.get(enricherId)==null) {
            LOGGER.info("Map enrichers is null.So fetching data from database");
            EnricherEntity enricherEntity = enricherManager.getEnricherEntity(enricherId);
            LOGGER.info("EnricherEntity from enrichers table is "+enricherEntity.toString());
            List<EnricherAttrEntity> enricherAttrEntities = enricherAttrManager.getEnricherAttrEntities(enricherId);
            LOGGER.info("Result Set of enricher_attr contains "+enricherAttrEntities.size()+" entities");
            Map<EnricherEntity,List<EnricherAttrEntity>> enricherAttributes=new HashMap<>();
            enricherAttributes.put(enricherEntity,enricherAttrEntities);
            enrichers.put(enricherId,enricherAttributes);
        }
        return enrichers.get(enricherId);
    }

    public EnricherResponse enrich(String id, String data, String enricherId){
        LOGGER.info("EnrichHandler.enrich() called with enricherId :"+enricherId);
        JSONObject jsonObject = new JSONObject();
        Map<EnricherEntity,List<EnricherAttrEntity>> enricherEntityMap = getEnricher(enricherId);
        for(Map.Entry<EnricherEntity,List<EnricherAttrEntity>> entry : enricherEntityMap.entrySet()){
            List<EnricherAttrEntity> enricherAttrEntityList = entry.getValue();
                for(EnricherAttrEntity enricherAttrEntity : enricherAttrEntityList){
                    String key = enricherAttrEntity.getKey();
                    String value = extractAttributes(enricherAttrEntity.getSourceKey(), data);
                    LOGGER.info("value for "+enricherAttrEntity.getSourceKey()+" = "+value);
                    jsonObject.put(key,value);
                }
        }

        EnricherEntry enricherEntry = null;
        try {
            enricherEntry = new EnricherEntry(id,objectMapper.readTree(jsonObject.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }


        EnricherEntity enricherEntity =enricherEntityMap.keySet().iterator().next();
        EnricherResponse enricherResponse =  getEnrichedDataForREST(enricherEntity.getUrl(), enricherEntry);
        LOGGER.info("EnricherResponse for enricherId "+enricherId+" is "+enricherResponse.toString());

        return enricherResponse;
    }

    private EnricherResponse getEnrichedDataForREST(String url, EnricherEntry enricherEntry){
        LOGGER.info("GetEnrichedDataForREST called with URL :"+url);
    	HttpHandler httpHandler=new HttpHandler();
Map<String,String> headers=new HashMap<>();
        headers.put("Content-Type","application/json");
        headers.put("Accept","application/json");

    	EnricherResponse  enricherResponse = httpHandler.post(enricherEntry, headers, url);
        LOGGER.info("EnricherResponse from Enricher with URL "+url+" is "+enricherResponse.toString());
        return enricherResponse;
    }

    public String extractAttributes(String sourceKey, String payload){
        LOGGER.info("Extracting attributes with sourceKey : "+sourceKey);
        JsonNode root = null;
        try {
            root = objectMapper.readTree(payload);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return root.get(sourceKey).toString();
    }

}
