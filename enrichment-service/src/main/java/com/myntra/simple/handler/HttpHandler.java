package com.myntra.simple.handler;

import java.util.Map;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.simple.client.entry.EnricherEntry;
import com.myntra.simple.client.entry.EnrichedProductEntry;
import com.myntra.simple.client.response.EnrichedProductResponse;
import com.myntra.simple.client.response.EnricherResponse;
import org.apache.log4j.Logger;

/**
 * Created by Riya Agarwal on 30/10/15.
 */

public class HttpHandler {
	private static final Logger LOGGER = Logger.getLogger(HttpHandler.class);


	public EnricherResponse post(EnricherEntry enricherEntry, Map<String, String> headers, String serviceUrl)  {
        LOGGER.info(enricherEntry.toString());
		LOGGER.info("Trying to hit URL:" + serviceUrl + " Payload=" + enricherEntry.toString() + " , Headers=" + headers);
		ContextInfo contextInfo = new ContextInfo();
		BaseWebClient client = null;
		try {
			client = new BaseWebClient(serviceUrl, ServiceURLProperty.MFS_URL, "application/json","application/json",contextInfo);
		} catch (ERPServiceException e) {
			e.printStackTrace();
		}
		for(Map.Entry<String,String> entry : headers.entrySet()) {
			client.setHeader(entry.getKey(), entry.getValue());
		}
		try {
			EnricherResponse enricherResponse =  client.post(enricherEntry, EnricherResponse.class);
            LOGGER.info("Enricher Info :"+enricherResponse.toString());
			return enricherResponse;
		} catch (ERPServiceException e) {
			LOGGER.fatal("ERPServiceException in HttpHandler");
			return null;
		}
	}

    public void post(EnrichedProductEntry enrichedEntry, Map<String, String> headers, String serviceUrl)  {
        LOGGER.info("Trying to hit URL:" + serviceUrl + " Payload=" + enrichedEntry.toString() + " , Headers=" + headers);
        ContextInfo contextInfo = new ContextInfo();
        BaseWebClient client = null;
        try {
            client = new BaseWebClient(serviceUrl, ServiceURLProperty.MFS_URL,"application/json","application/json", contextInfo);
        } catch (ERPServiceException e) {
            e.printStackTrace();
        }
        for(Map.Entry<String,String> entry : headers.entrySet()) {
            client.setHeader(entry.getKey(), entry.getValue());
        }
        try {
             client.post(enrichedEntry, EnrichedProductResponse.class);
            //return enricherResponse;
        } catch (ERPServiceException e) {
            LOGGER.fatal("ERPServiceException in HttpHandler");
          //  return null;
        }
    }
}
