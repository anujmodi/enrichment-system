package com.myntra.simple.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.simple.dao.ClientConfigDAO;
import com.myntra.simple.dao.ClientWorkflowDAO;
import com.myntra.simple.entity.ClientConfigEntity;
import com.myntra.simple.entity.ClientWorkflowEntity;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class ClientConfigDAOImpl extends BaseDAOImpl<ClientConfigEntity> implements ClientConfigDAO {
}
