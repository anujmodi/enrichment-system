package com.myntra.simple.dao;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.simple.entity.EnricherAttrEntity;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public interface EnricherAttrDAO extends BaseDAO<EnricherAttrEntity> {
}
