package com.myntra.simple.service;

import com.myntra.simple.client.response.GreetingResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Created by pavankumar.at on 27/07/15.
 */
@Path("/greeting/")
public interface GreetingService {

    @GET
    @Produces({"application/xml", "application/json"})
    @Path("/{name}")
    public GreetingResponse getGreeted(@PathParam("name") String name);

}
