package com.myntra.simple.service;

import com.myntra.simple.client.response.EnrichmentJobSubmitResponse;

import javax.ws.rs.*;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */
@Path("/enrichment")
public interface EnrichmentService {



    @POST
    @Path("/enrich")
    @Consumes("application/json")
    public EnrichmentJobSubmitResponse getEnriched(String message);
}
