package com.myntra.simple.service.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.simple.client.codes.SimpleErrorCodes;
import com.myntra.simple.client.codes.SimpleSuccessCodes;
import com.myntra.simple.client.entry.EnrichProductEntry;
import com.myntra.simple.client.response.EnrichmentJobSubmitResponse;
import com.myntra.simple.service.EnrichmentService;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */
public class EnrichmentServiceImpl implements EnrichmentService {

    private static final Logger LOGGER = Logger.getLogger(EnrichmentServiceImpl.class);

//    @Autowired
//    ProductEnrichmentQueuePublisher productEnrichmentQueuePublisher;

    @Autowired
    ObjectMapper objectMapper;


    /**
     *
     * @param message
     * @return
     *
     * Takes as input the message to be enriched, generates ClientReqID to track the request and passes on the message to Kafka
     * Publisher.
     */

    @Override
    public EnrichmentJobSubmitResponse getEnriched(String message) {

//        EnrichmentJobSubmitResponse enrichmentJobSubmitResponse = new EnrichmentJobSubmitResponse();
//        enrichmentJobSubmitResponse.setStatus(new StatusResponse(SimpleErrorCodes.GENERIC_ERROR, StatusResponse.Type.ERROR));
//
//        try{
//            Object productJson = objectMapper.readValue(message, Object.class);
//            Object clientIDObject = PropertyUtils.getProperty(productJson, "clientID");
//            Object data = PropertyUtils.getProperty(productJson,"message");
//            String clientReqID = clientIDObject.toString().concat(String.valueOf(System.currentTimeMillis()).concat(String.valueOf(Thread.currentThread().getId())));
//            // no kafkaproducerEntry made as of now.
//            PropertyUtils.setProperty(productJson, "clientReqID", clientReqID);
//            EnrichProductEntry enrichProductEntry = new EnrichProductEntry(clientIDObject.toString(),clientReqID,message.toString());
//            productEnrichmentQueuePublisher.publishMessage(enrichProductEntry);
//            enrichmentJobSubmitResponse.setStatus(new StatusResponse(SimpleSuccessCodes.SUCCESS, StatusResponse.Type.SUCCESS));
//            return enrichmentJobSubmitResponse;
//
//        }catch(JsonParseException e){
//            LOGGER.error(String.format("Failed to parse Json"));
//            enrichmentJobSubmitResponse.setStatus(new StatusResponse(SimpleErrorCodes.JSON_COULD_NOT_BE_PARSED, StatusResponse.Type.ERROR));
//            return enrichmentJobSubmitResponse;
//
//        } catch(IOException e){
//            LOGGER.error(String.format(" IO Exception "));
//            enrichmentJobSubmitResponse.setStatus(new StatusResponse(SimpleErrorCodes.IO_ERROR, StatusResponse.Type.ERROR));
//            return enrichmentJobSubmitResponse;
//        }catch(Exception e){
//            LOGGER.error(String.format("Failed to process message %s", message));
//            return enrichmentJobSubmitResponse;
//        }

        return null;

    }
}
