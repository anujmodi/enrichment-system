package com.myntra.simple.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Aditya Upadhyaya on 28/10/15.
 */
public class ConfigManager {

    private static ConfigManager configManager = new ConfigManager();

    private ConfigManager(){

    }

    public static ConfigManager getInstance(){
        return configManager;
    }


}
