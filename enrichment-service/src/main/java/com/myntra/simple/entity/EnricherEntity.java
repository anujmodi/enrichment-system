package com.myntra.simple.entity;

import com.myntra.commons.entities.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
@Entity
@Table(name = "enrichers")
public class EnricherEntity implements AbstractEntity{
	@Id
	@GeneratedValue
    @Column(name = "enricher_id")
    private String id;

    @Column(name = "enricher_url")
    private String url;

    @Column(name = "type")
    private String type;


    //hibernate requires default constructor
    public EnricherEntity(){

    }

    public EnricherEntity(String id, String url, String type) {
        this.id = id;
        this.url = url;
        this.type = type;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EnricherEntity{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
