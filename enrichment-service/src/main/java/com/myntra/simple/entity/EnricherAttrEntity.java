package com.myntra.simple.entity;

import com.myntra.commons.entities.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
@Entity
@Table(name = "enricher_attr")
public class EnricherAttrEntity implements AbstractEntity {

	@Id
	@GeneratedValue
    @Column(name = "id")
    String id;
	
	@Column(name = "enricher_id")
    String enricherId;


    @Column(name = "key_name")
    String key;

    @Column(name = "source_key")
    String sourceKey;


    //hibernate requires default constructor
    public EnricherAttrEntity(){

    }

    public EnricherAttrEntity(String enricherId,  String key, String sourceKey) {
        this.enricherId =enricherId;
        this.key = key;
        this.sourceKey = sourceKey;
    }

    @Override
    public String getId() {
        return enricherId;
    }

    public void setId(String enricherId) {
        this.enricherId = enricherId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    @Override
    public String toString() {
        return "EnricherAttrEntity{" +
                "enricherId='" + enricherId + '\'' +
                ", key='" + key + '\'' +
                ", sourceKey='" + sourceKey + '\'' +
                '}';
    }
}

