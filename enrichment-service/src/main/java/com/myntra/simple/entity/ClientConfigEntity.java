package com.myntra.simple.entity;

import com.myntra.commons.entities.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
@Entity
@Table(name = "client_configs")
public class ClientConfigEntity implements  AbstractEntity {
	@Id
	@GeneratedValue
	@Column(name = "id")
	String id;

	@Column(name = "client_id")
	String clientId;

	@Column(name = "client_name")
	String name;

	@Column(name = "response_url")
	String responseUrl;


	public ClientConfigEntity() {
		// TODO Auto-generated constructor stub
	}

	public ClientConfigEntity(String clientId, String name, String responseUrl) {
		this.clientId = clientId;
		this.name = name;
		this.responseUrl = responseUrl;
	}

	@Override
	public String getId() {
		return clientId;
	}

	public void setId(String clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResponseUrl() {
		return responseUrl;
	}

	public void setReponseURL(String responseUrl) {
		this.responseUrl = responseUrl;
	}


	@Override
	public String toString() {
		return "ClientWorkflowEntity{" + "clientId='" + clientId + '\'' + ", name='" + name + '\'' + ", responseUrl='" + responseUrl+'}';
	}


}
