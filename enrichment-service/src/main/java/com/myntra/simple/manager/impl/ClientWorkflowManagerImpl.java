package com.myntra.simple.manager.impl;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.simple.client.entry.EnrichProductEntry;
import com.myntra.simple.entity.ClientWorkflowEntity;
import com.myntra.simple.manager.ClientWorkflowManager;
import com.myntra.simple.manager.EnricherManager;

/**
 * Created by Aditya Upadhyaya on 26/10/15.
 */
public class ClientWorkflowManagerImpl extends BaseManagerImpl<EnrichProductEntry,ClientWorkflowEntity> implements ClientWorkflowManager {

    private static final Logger LOGGER = Logger.getLogger(ClientWorkflowManagerImpl.class);

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public List<ClientWorkflowEntity> getWorkflow(String clientId) throws ManagerException {
        String query = "SELECT * FROM client_workflow WHERE client_id = "+clientId+";";
        LOGGER.info(query);
        List<ClientWorkflowEntity> list = dao.getAllObjectsForQuery(query);
        LOGGER.info("ResultSet size for client_workflow is "+list.size());
        Collections.sort(list);

        return list;

    }



}
