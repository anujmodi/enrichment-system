package com.myntra.simple.manager;

import com.myntra.commons.manager.BaseManager;
import com.myntra.simple.client.entry.EnricherEntry;
import com.myntra.simple.entity.EnricherEntity;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public interface EnricherManager extends BaseManager<EnricherEntry,EnricherEntity> {

    public EnricherEntity getEnricherEntity(String enricherID);
}
