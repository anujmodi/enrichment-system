package com.myntra.simple.manager.impl;

import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.simple.client.entry.EnricherEntry;
import com.myntra.simple.entity.EnricherEntity;
import com.myntra.simple.manager.EnricherAttrManager;
import com.myntra.simple.manager.EnricherManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Aditya Upadhyaya on 30/10/15.
 */
public class EnricherManagerImpl extends BaseManagerImpl<EnricherEntry,EnricherEntity> implements EnricherManager {

    @Autowired
    EnricherAttrManager enricherAttrManager;

    public EnricherEntity getEnricherEntity(String enricherID){

        EnricherEntity enricherEntity = dao.getObjectForQuery("SELECT * FROM enrichers WHERE enricher_id = '"+enricherID+"';");
        return enricherEntity;
    }
}
