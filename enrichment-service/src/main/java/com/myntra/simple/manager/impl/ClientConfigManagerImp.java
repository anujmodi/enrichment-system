package com.myntra.simple.manager.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.commons.exception.ManagerException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.simple.client.entry.EnrichProductEntry;
import com.myntra.simple.entity.ClientConfigEntity;
import com.myntra.simple.manager.ClientConfigManager;
import com.myntra.simple.manager.EnricherManager;


public class ClientConfigManagerImp extends BaseManagerImpl<EnrichProductEntry,ClientConfigEntity> implements ClientConfigManager {

    private static final Logger LOGGER = Logger.getLogger(ClientConfigManagerImp.class);

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public ClientConfigEntity getConfig(String clientId) throws ManagerException {
        String query = "SELECT * FROM client_configs WHERE client_id = "+clientId+";";
        LOGGER.info(query);
        ClientConfigEntity clientConfig = dao.getObjectForQuery(query);
        LOGGER.info("Returning clientConfig:"+clientConfig);
        return clientConfig;

    }



}
